//
// Created by Osea on 2020/10/24.
//

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

struct Node
{
    string data1;
    string data2;
    int num1{};
    int num2{};
    Node *next{};
};

class LinkList
{
protected:
    Node *head, *tail;
public:
    LinkList();
    void add_node(const string& data1, const string& data2, int num1, int num2);
    void display();
    void front(const string& data1, const string& data2, int num1, int num2);
    void deletion(int index);
    void insert_after(int index, const string &data1, const string &data2, int num1, int num2);
    [[nodiscard]] int search(const string &data1, const string &data2, int num1, int num2) const;
    void edit(int index, const string &data1, const string &data2, int num1, int num2);
    Node *get_data(int index);
    int length;
};

LinkList::LinkList() {
    head = nullptr;
    tail = nullptr;
    length = 0;
}
void LinkList::add_node(const string &data1, const string &data2, int num1, int num2) {
    Node *tmp = new Node;
    tmp->data1 = data1;
    tmp->data2 = data2;
    tmp->num1 = num1;
    tmp->num2 = num2;
    tmp->next = nullptr;
    if(head == nullptr)
    {
        head = tmp;
        tail = tmp;
    }
    else {
        tail->next = tmp;
        tail = tail->next;
    }
    length++;
}
void LinkList::display() {
    Node *tmp;
    tmp = head;
    while (tmp != nullptr)
    {
        cout << tmp->data1 << '\t' << tmp->data2 << '\t' << tmp->num1 << '\t' << tmp->num2 << endl;
        tmp = tmp->next;
    }
    cout << "Total length: " << length << endl;
}

void LinkList::front(const string &data1, const string &data2, int num1, int num2) {
    Node *tmp = new Node;
    tmp->data1 = data1;
    tmp->data2 = data2;
    tmp->num1 = num1;
    tmp->num2 = num2;
    tmp->next = head;
    head = tmp;
    length++;
}

void LinkList::deletion(int index) {
    if (index < 1 || index > length)
    {
        cout << "IndexError!" << endl;
        return;
    }
    else
    {
        Node *p = head, *q;
        int count;
        for (count = 1; count < index; count++)
            p = p->next;
        q = p->next;
        p->next = q->next;
        cout << "Delete Node at: " << index << ", content: " << q->data1 << '\t' << q->data2 << '\t' << q->num1
             << '\t' << q->num2 << endl;
        length--;
        delete q;
        return;
    }
}

void LinkList::insert_after(int index, const string &data1, const string &data2, int num1, int num2) {
    if (index < 1 || index > length)
    {
        cout << "IndexError!" << endl;
        return;
    }
    else
    {
        Node *p = head;
        for (int i = 1; i < index; i++) {
            p = p->next;
        }
        Node *tmp = new Node;
        tmp->data1 = data1;
        tmp->data2 = data2;
        tmp->num1 = num1;
        tmp->num2 = num2;
        tmp->next = p->next;
        p->next = tmp;
        length++;
        return;
    }
}

int LinkList::search(const string &data1, const string &data2, int num1, int num2) const {
    Node *p = head;
    int count = 1;
    while (p != nullptr)
    {
        if (p->data1==data1&&p->data2==data2&&p->num1==num1&&p->num2==num2)
        {
            return count;
        }
        count++;
        p = p->next;
    }
    return 0;
}

void LinkList::edit(int index, const string &data1, const string &data2, int num1, int num2) {
    if (index < 1 || index > length)
    {
        cout << "IndexError!" << endl;
        return;
    }
    else
    {
        Node *p = head;
        for (int i = 1; i < index; i++) {
            p = p->next;
        }
        cout << "Before Edit: ";
        cout << p->data1 << '\t' << p->data2 << '\t' << p->num1 << '\t' << p->num2 << endl;
        p->data1 = data1;
        p->data2 = data2;
        p->num1 = num1;
        p->num2 = num2;
        cout << "After Edit: ";
        cout << p->data1 << '\t' << p->data2 << '\t' << p->num1 << '\t' << p->num2 << endl;
        return;
    }
}

Node *LinkList::get_data(int index) {
    if (index < 1 || index > length)
    {
        cout << "IndexError!" << endl;
        return nullptr;
    }
    else
    {
        Node *p = head;
        for (int i = 1; i < index; i++) {
            p = p->next;
        }
        return p;
    }
}


int main()
{
    char c = '*';
    LinkList a;
    string string1, string2;
    int num1, num2, i;
    ifstream file;
    file.open("1.txt");
    if(file.is_open())
    {
        string line;
        while(getline(file, line))
        {
            istringstream iss(line);
            iss >> string1 >> string2 >> num1 >> num2;
            a.add_node(string1, string2, num1, num2);
        }
        file.close();
    }
    ofstream ofs;
    ofs.open("1.txt", ofstream::out | ofstream::trunc);
    while (c != '0')
    {
        cout << "1.Display LinkList" << endl;
        cout << "2.Insert Node" << endl;
        cout << "3.Delete Node" << endl;
        cout << "4.Search Node" << endl;
        cout << "5.Edit Node" << endl;
        cout << "0.Save and Exit" << endl;
        cout << "Enter index:";
        cin >> c;
        switch (c) {
            case '0':
                if (ofs.is_open())
                {
                    for (int j = 1; j <= a.length; j++) {
                        ofs << a.get_data(j)->data1;
                        ofs << ' ';
                        ofs << a.get_data(j)->data2;
                        ofs << ' ';
                        ofs << a.get_data(j)->num1;
                        ofs << ' ';
                        ofs << a.get_data(j)->num2;
                        ofs << '\n';
                    }
                    ofs.close();
                }
                break;
            case '1':
                a.display();
                break;
            case '2':
                cout << "Enter Index:";
                cin >> i;
                cout << "Enter Content:";
                cin >> string1 >> string2 >> num1 >> num2;
                if (i!=0)
                {
                    a.insert_after(i, string1, string2, num1, num2);
                    a.display();
                }
                else if(i==0)
                {
                    a.front(string1, string2, num1, num2);
                    a.display();
                }
                break;
            case '3':
                cout << "Enter Index:";
                cin >> i;
                a.deletion(i);
                a.display();
                break;
            case '4':
                cout << "Enter Content:";
                cin >> string1 >> string2 >> num1 >> num2;
                int result;
                result = a.search(string1, string2, num1, num2);
                if (result!=0)
                    cout << "Target found at: " << result << endl;
                else
                    cout << "Target NOT FOUND!" << endl;
                break;
            case '5':
                cout << "Enter Index:";
                cin >> i;
                cout << "Enter Content:";
                cin >> string1 >> string2 >> num1 >> num2;
                a.edit(i, string1, string2, num1, num2);
                break;
        }
    }
    return 0;
}
